package main

import "fmt"

type speaker interface {
	speak()
}

type Dog struct {
	name        string
	is_mammal   bool
	pack_factor int
}

func (d *Dog) speak() {
	fmt.Printf("This is Dog, 	its name is %s, 	mammal check: %t, 	factor: %d\n", d.name, d.is_mammal, d.pack_factor)
}

type Cat struct {
	name         string
	is_mammal    bool
	climb_factor int
}

func (c *Cat) speak() {
	fmt.Printf("This is Cat, 	its name is %s, 	mammal check: %t, 	factor: %d\n", c.name, c.is_mammal, c.climb_factor)
}

func main() {
	speakers := []speaker{
		&Dog{
			"Fido", true, 5,
		},
		&Cat{
			name: "catto", is_mammal: true, climb_factor: 5,
		},
	}

	for _, s := range speakers {
		s.speak()
	}

}
