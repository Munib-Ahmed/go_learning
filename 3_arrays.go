package main

import "fmt"

func main() {

	fmt.Println("3.2 Arrays")

	var fruits [5]string
	fruits[0] = "apple"
	fruits[1] = "mango"
	fruits[2] = "banana"
	fruits[3] = "orange"
	fruits[4] = "grapes"
	for i, fruit := range fruits {
		fruits[1] = "potato"
		fmt.Println(i, fruit, &fruit, &fruits, &fruits[i])
		//potato is not updated b/c fruit works on value i-e it makes a copy of the array and iterate over that
	}

	for i, fruit := range &fruits {
		fruits[1] = "potato"
		fmt.Println(i, fruit)
		// thinks get updated b/c this time copy of pointer to array is created
	}

	numbers := [4]int{01, 10, 20, 30}
	for i := 0; i < len(numbers); i++ {
		fmt.Println(i, numbers[i])
	}
	fmt.Println("3.3 SLices")
	fmt.Println("capacity can be greater than length")
	// if we know the requirement, we can use make command as below
	slice_fruits := make([]string, 5, 8) // 5 zero valued strings and at the end 8 actual strings. Thus, length = 0 suggested
	slice_fruits[0] = "apple"            //to assign value like this length should be provided ie slice_fruits[5] = "peach" will be error b/c length is 5
	slice_fruits[1] = "mango"
	slice_fruits[2] = "banana"
	slice_fruits[3] = "orange"
	slice_fruits[4] = "grapes"
	slice_fruits = append(slice_fruits, "peach")
	slice_fruits = append(slice_fruits, "strawberry")
	slice_fruits = append(slice_fruits, "apricot")

	inspectslice(slice_fruits)
	fmt.Println(len(slice_fruits), cap(slice_fruits), slice_fruits)

	//	if we don't know the requirement, we can just initialize the slice and append it as below
	//var slice_fruits []string

	slice_fruits_2 := slice_fruits[2:4]
	// hint: check the address, if address are same slice points to same data
	// now changing the slice_fruits_2 will affect the original (slice_fruits)
	// b/c only a new slice pointer, length & capacity object is made
	// not a seperate slice
	// not matter how you change through keyword "append" or indexing it will affect the original
	inspectslice(slice_fruits_2)

	// but their are work arounds
	// this one is discovered by myself, not sure if its authentic or not but works
	println("methods\n\n")
	slice_fruits_3 := append(slice_fruits, "melon") //if we assign value through : append(slice_fruits[2:4], "melon") it will fail
	slice_fruits_3 = append(slice_fruits_3, "water-melon")
	slice_fruits = append(slice_fruits, "poison")
	inspectslice(slice_fruits_3)
	inspectslice(slice_fruits)

	//another way
	//specify the capacity, we know new slice has no capacity b/c data is already stored in next data address, so it will reallocate
	slice_fruits_4 := slice_fruits[2:5:5]
	inspectslice(slice_fruits_4)
	slice_fruits_4 = append(slice_fruits_4, "blueberry")
	inspectslice(slice_fruits_4)
	inspectslice(slice_fruits)

	//another way
	//use builtin keyword "copy"
	slice_fruits_5 := make([]string, len(slice_fruits))
	copy(slice_fruits_5, slice_fruits)
	slice_fruits_5 = append(slice_fruits_5, "dragon-fruit")
	inspectslice(slice_fruits_5)
	inspectslice(slice_fruits)

}

func inspectslice(slice []string) {
	fmt.Println(len(slice), cap(slice), slice)
	// in cases of arrays and slices, changing them will cost the values in the main to change[passing by reference]
	for i, s := range slice {
		fmt.Println(i, &slice[i], s)
	}
}
