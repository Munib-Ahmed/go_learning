package main

import "fmt"

// Creating an interface
// interface should describe behaviour
type tank interface {
	// Methods
	Tarea() float64
	Volume() float64
}

type myvalue struct {
	radius float64
	height float64
}

// Implementing methods of the tank interface
func (m myvalue) Tarea() float64 {
	return 2*m.radius*m.height + 2*3.14*m.radius*m.radius
}

func (m myvalue) Volume() float64 {
	return 3.14 * m.radius * m.radius * m.height
}

func function_assertion(a interface{}) {
	// Extracting the value of a
	val := a.(string)
	fmt.Println("Value: ", val)
}

func function_assertion_bool(a interface{}) {
	value, ok := a.(float64)
	fmt.Println(value, ok)
}

// Interface - polymorphism
type employee interface {
	develop() int
	name() string
}

// Structure 1
type team1 struct {
	totalapp_1 int
	name_1     string
}

// Methods of employee interface are
// implemented by the team1 structure
func (t1 team1) develop() int {
	return t1.totalapp_1
}

func (t1 team1) name() string {
	return t1.name_1
}

// Structure 2
type team2 struct {
	totalapp_2 int
	name_2     string
}

// Methods of employee interface are
// implemented by the team2 structure
func (t2 team2) develop() int {
	return t2.totalapp_2
}

func (t2 team2) name() string {
	return t2.name_2
}

func finaldevelop(i []employee) {

	totalproject := 0
	for _, ele := range i {

		fmt.Printf("\nProject environment = %s\n ", ele.name())
		fmt.Printf("Total number of project %d\n ", ele.develop())
		totalproject += ele.develop()
	}
	fmt.Printf("\nTotal projects completed by "+
		"the company = %d", totalproject)
}

type printer interface {
	print_value()
	print_pointer()
}

type user struct {
	name string
}

func (u user) print_value() {
	fmt.Println("Name: %s\n", u.name)
}

func (u *user) print_pointer() {
	fmt.Println("Name: %s\n", u.name)
}

// Main Method
func main() {

	// Accessing elements of the tank interface
	var t tank
	t = myvalue{10, 14}
	fmt.Println("Area of tank :", t.Tarea())
	fmt.Println("Volume of tank:", t.Volume())

	// type assertion
	var val interface {
	} = "GeeksforGeeks"
	function_assertion(val)

	//type assertion with bool
	var a1 interface {
	} = 98.09
	function_assertion_bool(a1)

	var a2 interface {
	} = "GeeksforGeeks"
	function_assertion_bool(a2)

	// Interface - polymorphism
	res1 := team1{totalapp_1: 20, name_1: "Android"}
	res2 := team2{totalapp_2: 35, name_2: "IOS"}
	final := []employee{res1, res2}
	finaldevelop(final)

	// interface & polymorphism with video
	println("\nprint by value & print by pointer")
	var u user
	u = user{name: "bill"}
	// if we have reference of an object as &u below then we can use either print_value or print_pointer
	// but if we have object then we can not call print_pointer
	// in short, through pointer we call whatever we want, but with value we can only call value
	entities := []printer{
		&u, &u,
		//u, &u,
	}

	u.name = "bill_chill"

	for _, e := range entities {
		e.print_value()
		e.print_pointer()
	}

}
