package main

import (
	"fmt"
	"sort"
)

type user struct {
	name    string
	surname string
}

func main() {

	fmt.Println("3.4 Maps")
	users := make(map[string]user)

	users["roy"] = user{"rob", "roy"}
	users["ford"] = user{"henry", "ford"}
	users["jackson"] = user{"michael", "jackson"}

	for key, value := range users {
		fmt.Println(key, value)
	}
	fmt.Println()

	for key := range users {
		fmt.Println(key)
	}
	fmt.Println("\nsorting")
	var keys []string
	for key := range users {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key := range keys {
		fmt.Println(key, users[key])
	}

	fmt.Println("\ndelete & found")
	u_1, found_1 := users["roy"]
	fmt.Println("roy", found_1, u_1)

	delete(users, "roy")

	u_2, found_2 := users["roy"]
	fmt.Println("roy", found_2, u_2)

}
