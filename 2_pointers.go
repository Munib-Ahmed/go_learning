package main

import (
	"fmt"
	"unsafe"
)

func increment_by_value(inc int) {
	inc++
	fmt.Println("inc:", inc, "		&inc:", &inc)
}
func increment_by_reference(inc *int) {
	*inc++
	fmt.Println("inc:", inc, "	*inc:", *inc, "		&inc:", &inc)
}

type user struct {
	name  string
	email string
}

func main() {

	fmt.Println("Pointers")
	var counter int
	counter = 10

	fmt.Println("Counter:", counter, "		&Counter:", &counter)

	increment_by_value(counter)
	fmt.Println("Counter:", counter, "		&Counter:", &counter)

	increment_by_reference(&counter)
	fmt.Println("Counter:", counter, "		&Counter:", &counter)

	fmt.Println("\n 2.3 - 	escape analysis		\n")
	u1 := create_user_v1()
	u2 := create_user_v2()
	fmt.Println("&u1:", &u1, "	&u2:", &u2)

	fmt.Println("\n 2.4 - 	COnstants		\n")
	//	2 types of constants 1. kind 2. type
	//	kind constant can be implicitly converted by the compiler

	var answer = 3 * 0.3333 // kindfloat(3)*kindfloat(0.333)
	fmt.Println("answer= ", answer)
	var third = 1 / 3.0 // kindfloat(1)*kindfloat(3.0)
	fmt.Println("third= ", third)
	var zero = 1 / 3 // kindint(1)*kindint(3)
	fmt.Println("zero= ", zero)

	const one int8 = 1
	const two = 2 * one
	fmt.Println("two=", two)

	//const two_2 = 2222222 * one
	//fmt.Println("two_2", two_2)
	//this will give error b/c one and two_2 dont have same type

	type duration int64
	const (
		nano   duration = 1
		micro           = 1000 * nano
		milli           = 1000 * micro
		second          = 1000 * milli
		minute          = 60 * second
		hour            = 60 * minute
		day             = 24 * hour
		year            = 365 * day
	)
	fmt.Printf("Size of an int:", unsafe.Sizeof(year))
	//take a look at: iota

}

// Fractory functions are functions that creates a value, initialize it for you & return it back to caller
func create_user_v1() user {
	u := user{
		name:  "Munib Ahmed",
		email: "munib.ahmed@emumba.com",
	}
	fmt.Println("u:", u, "	&u:", &u)
	return u
}
func create_user_v2() *user {
	u := user{ //u:=&user{
		name:  "Munib Ahmed",
		email: "munib.ahmed@emumba.com",
	}
	fmt.Println("u:", u, "	&u:", &u)
	return &u //u
	//if we replace the code with commented one, working will be same but readability is effected
	//in general, DOn't use pointer semantics in construction, but value semantics
}
