package main

type notifier interface {
	notify()
}

type user struct {
	name  string
	email string
}

func (u *user) notify() {
	println("user notification:	Name: %s, Email: %s", u.name, u.email)
}

// if we uncomment it, this function will be used [just uncomment it and run it, you will get the idea]
//func (a *admin_2) notify() {
//	println("admin notification:	Name: %s, Email: %s", a.name, a.email)
//}

type admin struct {
	person user //Not Embedded: this is simply using one struct in another
	level  string
}
type admin_2 struct {
	user  // Embedded: we embedded the user into admin_2
	level string
}

func send_notification(n notifier) {
	n.notify()
}

func main() {
	ad := admin{
		person: user{
			name:  "munib",
			email: "munib.ahmed",
		},
		level: "super",
	}

	ad.person.notify()
	ad_2 := admin_2{
		user: user{
			name:  "munib",
			email: "munib.ahmed",
		},
		level: "super",
	}
	ad_2.user.notify()
	ad_2.notify()
	send_notification(&ad_2)
	send_notification(&ad_2.user)

}
