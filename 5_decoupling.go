package main

import "fmt"

type user struct {
	name  string
	email string
}

// method with a value receiver
func (u user) notify() {
	fmt.Println("notification: %s<%s>", u.name, u.email)
}

// method with a pointer receiver
func (u *user) change_email(email string) {
	u.email = email
}

func main() {
	fmt.Println("4.1	Declare & Receive Behaviour")
	fmt.Println("no matter if this by pointer or value we can call method using receivers of pointers or values")

	//Value of type user can be used to call methods
	//declared with both value & pointer receiver
	bill := user{"bill", "bill@emumba"}
	bill.notify()
	bill.change_email("bill@emumba.com")
	bill.notify()

	fmt.Println("")
	//Pointers of type user can be used to call methods
	//declared with both value & pointer receiver
	john := user{"john", "john@emumba"}
	john.notify()
	john.change_email("john@emumba.com")
	john.notify() //just not a good practice, we will learn more in coming lectures

	fmt.Println("\n Values and Pointer Semantics")
	fmt.Println("when to use Values or Pointer Semantics")
	fmt.Println("Using wrong semantics can cause a lot of pain, let's see how it is\n")

	users := []user{
		{"bill", "bill@emumba.com"},
		{"john", "john@emumba.com"},
	}

	// iterate over slice, using valve semantics
	for _, u := range users {
		u.notify()
		u.change_email("abcd")
		u.notify()
		println()
	}

	//Actual slice values
	for _, u := range users {
		u.notify()
	}

	//	FUnction/Methods Variables
	println("\nFUnction/Methods Variables - Decoupling")
	user_func_1 := bill.notify
	user_func_1()
	bill.name = "some random name"
	user_func_1()
	// here we changed bill name but, it did not worked out
	// because "user_func_1" is assigned bill.notify and notify used value semantics
	// Thus, "user_func_1" will make a copy of the code(function) and object. Operating on copy won't effect orginal one
	// But this is not the case if function uses pointer semantics, as shown below
	user_func_2 := bill.change_email
	bill.notify()
	user_func_2("bill@gmail.com")
	bill.notify()

	//	in any case, heap will be used beacuse
	//	user_func_2 points to function and the object, in cases when it is pointing to 2 thinks escape analysis can't decide to place it on stack. thus, heap
	// uses decoupling if neccessary beacuse avoiding heap is prefered
}
