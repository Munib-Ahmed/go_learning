package main

import (
	"fmt"
	"unsafe"
)

type example struct {
	counter int
	pi      float32
	flag1   bool
	flag2   bool
}
type example2 struct {
	counter int
	pi      float32
	flag1   bool
	flag2   bool
}
type example3 struct {
	pi      float32
	flag1   bool
	flag2   bool
	flag3   bool //currently, size is 12, if I comment this line size will be 8
	counter uint16
}

func main() {
	var info example
	const infoSize = unsafe.Sizeof(info)

	fmt.Println("struct_size:", infoSize)

	var e1 example
	e1.counter = 1

	fmt.Printf("%+v\n", e1)

	e2 := example{
		counter: 10,
		pi:      3.141592,
		flag1:   true,
		flag2:   false,
	}

	//e2 := example{}, same as "var e1 example"
	fmt.Println("flag:", e2.flag1)
	fmt.Println("flag:", e2.flag2)
	fmt.Println("counter:", e2.counter)
	fmt.Println("pi:", e2.pi)

	//literals <declaring variable with ":=">
	fmt.Println("::e3::")
	var e3 struct {
		flag    bool
		counter int16
		pi      float32
	}

	//same as above
	//e3 := struct {
	//	flag    bool
	//	counter int16
	//	pi      float32
	//}{
	//	flag:    true,
	//	counter: 10,
	//	pi:      3.141592,
	//}

	fmt.Printf("%+v\n", unsafe.Sizeof(e3))

	//example and example2 are identical, but are seperate structs
	var ex1 example
	var ex2 example2
	ex2 = example2{counter: 10}

	//if we want to convert one form to another
	ex1 = example(ex2)
	//if ex2 was anonymous literal then we don't need explicit conversion i-e we don't need example keyword <in above line>
	// in easy words, if we want to convert a variable which belongs to a specific type then we need name of type in which we want to convert
	// if the type is anonymous we can convert it without type in which we want to convert

	fmt.Printf("%+v\n", ex1)

	var ex3 example3
	ex3.counter = 10
	fmt.Printf("Size of an int:", unsafe.Sizeof(ex3.counter))
	fmt.Printf("%+v\n", unsafe.Sizeof(ex3))

}
